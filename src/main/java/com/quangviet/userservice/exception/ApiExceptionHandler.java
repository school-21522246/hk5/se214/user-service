package com.quangviet.userservice.exception;

import com.quangviet.userservice.dto.response.ExceptionDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ControllerAdvice
public class ApiExceptionHandler {
    @ExceptionHandler(value = {ApiExceptionEntity.class})
    public ResponseEntity<Object> handleRequestException(ApiExceptionEntity apiExceptionEntity) {
        return ResponseEntity
                .status(apiExceptionEntity.httpStatus)
                .body(ExceptionDTO.builder()
                                .id(apiExceptionEntity.id)
                                .message(apiExceptionEntity.message)
                        .build());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<ExceptionDTO> dtos = new ArrayList<>();
        ValidiationExceptionEntity res = new ValidiationExceptionEntity("","",null);
        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        errors.forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();

            if (errorMessage != null &&
                    res.getId().isBlank() &&
                    (errorMessage.toLowerCase().contains("null") ||
                        errorMessage.toLowerCase().contains("empty"))) {
                res.setId(ApiExceptionType.missingField.id);
                res.setMessage(ApiExceptionType.missingField.message);
            }
            dtos.add(ExceptionDTO.builder()
                            .id(fieldName)
                            .message(errorMessage)
                    .build());
        });
        if (dtos.size() == 1) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(dtos.stream().findFirst().get());
        }
        res.setDetails(dtos);
        return ResponseEntity
                .status(ApiExceptionType.missingField.httpStatus)
                .body(res);
    }
}
